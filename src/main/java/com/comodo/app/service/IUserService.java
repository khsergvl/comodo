package com.comodo.app.service;

import com.comodo.app.entity.UserEntity;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface IUserService {
    UserEntity createUser(UserEntity user);

    Optional<UserEntity> fetchUser(int id);
    UserEntity updateUser(UserEntity user);
    boolean removeUser(int id);
    Page<UserEntity> getPage(int pageNumber);
}
