package com.comodo.app.service.impl;

import com.comodo.app.Constants;
import com.comodo.app.entity.UserEntity;
import com.comodo.app.repository.UserRepository;
import com.comodo.app.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserEntity createUser(UserEntity user) {
        try {
            return userRepository.save(user);
        } catch (Exception e) {
            log.warn("CRUD user operation exception", e);
            return null;
        }
    }

    @Override
    public Optional<UserEntity> fetchUser(int id) {
        try {
            return userRepository.findById(id);
        } catch (Exception e) {
            log.warn("CRUD user operation exception", e);
            return Optional.empty();
        }
    }

    @Override
    public UserEntity updateUser(UserEntity user) {
        try {
            return userRepository.save(user);
        } catch (Exception e) {
            log.warn("CRUD user operation exception", e);
            return null;
        }
    }

    @Override
    public boolean removeUser(int id) {
        try {
            userRepository.deleteById(id);
            return !userRepository.findById(id).isPresent();
        } catch (Exception e) {
            log.warn("CRUD user operation exception", e);
            return false;
        }
    }

    @Override
    public Page<UserEntity> getPage(int pageNumber) {
        try {
            return userRepository.findAll(PageRequest.of(pageNumber, Constants.USER_PAGINATION_PAGE_SIZE, Sort.Direction.ASC, "id"));
        } catch (Exception e) {
            log.warn("CRUD user operation exception", e);
            return null;
        }
    }
}
