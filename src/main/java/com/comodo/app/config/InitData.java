package com.comodo.app.config;

import com.comodo.app.entity.UserEntity;
import com.comodo.app.model.Gender;
import com.comodo.app.service.impl.UserService;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.time.LocalDate;

@Configuration
public class InitData {

    private UserService userService;

    public InitData(UserService userService) {
        this.userService = userService;
    }

    @PostConstruct
    private void dataInit() {

        UserEntity user = new UserEntity();
        user.setFirstName("Alex");
        user.setLastName("Eris");
        String[] userEmail = {"test@test.com", "test@test.com"};
        user.setEmails(userEmail);
        user.setBirthday(LocalDate.now().minusDays(10));
        user.setGender(Gender.MALE);
        userService.createUser(user);

        UserEntity user2 = new UserEntity();
        user2.setFirstName("Julia");
        user2.setLastName("Eris");
        String[] user2Email = {"test@test.com", "test@test.com"};
        user2.setEmails(user2Email);
        user2.setBirthday(LocalDate.now().minusDays(5));
        user2.setGender(Gender.FEMALE);
        userService.createUser(user2);
    }

}
