package com.comodo.app.controller;

import com.comodo.app.entity.UserEntity;
import com.comodo.app.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;


@Controller
@Slf4j
public class UsersController {

    private IUserService userService;

    public UsersController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = {"/", "/index"})
    public String indexPage(Model model) {
        Page<UserEntity> indexPageUsers = userService.getPage(0);
        model.addAttribute("users", indexPageUsers.getContent());
        model.addAttribute("isNeedPagination", indexPageUsers.hasNext());
        model.addAttribute("pageNumber", indexPageUsers.getNumber() + 1);
        return "index";
    }

    @GetMapping(value = {"/index/{id}"})
    public String pageNumber(@PathVariable("id") int id, Model model) {
        Page<UserEntity> indexPageUsers = userService.getPage(id);
        model.addAttribute("users", indexPageUsers.getContent());
        model.addAttribute("isNeedPagination", indexPageUsers.hasNext());
        model.addAttribute("pageNumber", indexPageUsers.getNumber() + 1);
        return "page";
    }



    @PostMapping(value = "/user")
    public ResponseEntity<UserEntity> create(@RequestBody UserEntity user) {
        log.info("Create user request ", user);
        UserEntity savedUser = userService.createUser(user);
        if (Optional.ofNullable(savedUser).isPresent()) {
            return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping(value = "/user/{id}")
    public ResponseEntity<UserEntity> fetch(@PathVariable("id") int id) {
        log.info("Fetch user={} request ", id);
        if ((id <= 0)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Optional<UserEntity> savedUser = userService.fetchUser(id);
        if (savedUser.isPresent()) {
            return new ResponseEntity<>(savedUser.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = "/user/{id}")
    public ResponseEntity<UserEntity> update(@PathVariable("id") int id, @RequestBody UserEntity user) {
        log.info("Update id={} user={} request ", id, user);
        if ((user.getId() != id) || (id <= 0)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        UserEntity savedUser = userService.updateUser(user);
        if (Optional.ofNullable(savedUser).isPresent()) {
            return new ResponseEntity<>(savedUser, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/user/{id}")
    public ResponseEntity<Void> remove(@PathVariable("id") int id) {
        log.info("Remove user={} request ", id);
        if (userService.removeUser(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
