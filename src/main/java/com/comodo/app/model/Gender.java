package com.comodo.app.model;

public enum Gender {
    MALE, FEMALE
}
