package com.comodo.app;

public class Constants {
    private Constants() {

    }

    public static final int USER_PAGINATION_PAGE_SIZE = 5;
}
