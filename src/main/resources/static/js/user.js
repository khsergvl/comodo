addUser = function (event) {
    event.preventDefault();
    var formData = new FormData(document.forms.createform);
    var data = {};
    for (var key of formData.keys()) {
        if (key == "emails") {
            data[key] = formData.get(key).split(" ");
        } else {
            data[key] = formData.get(key);
        }
        data["gender"] = $("#gender-input option:selected").text();
    }

    var jsonForm = JSON.stringify(data);
    $.ajax({
        url: "/user",
        type: "POST",
        enctype: "multipart/form-data",
        processData: false,
        contentType: "application/json",
        data: jsonForm,
        success: function (user) {
            swal({
                position: 'top-right',
                icon: 'success',
                title: 'User Created',
                button: false,
                timer: 1500
            });

            if (!$('#users-paginate-button-block').length) {
                var userID = user.id;
                $("#users-table-body").append(`<tr id=\'${user.id}\' class=\'user-head\'>`);
                $("#" + userID).append(buildUserHtml(user));
                $(".user-record").on("blur", updateUser);
                $(".user-remove").on("click", removeUser);
            }

        },
        error: function (xhr, resp, text) {
            swal({
                position: 'top-right',
                icon: 'error',
                title: 'Error',
                button: false,
                timer: 1500
            })
        }
    })
};

function buildUserHtml(user) {
    var userRecordHtml = "";
    userRecordHtml = userRecordHtml.concat(`<td data-name='id'>${user['id']}</td>`);
    userRecordHtml = userRecordHtml.concat(`<td data-name='firstName' class='user-record' contenteditable='true'>${user['firstName']}</td>`);
    userRecordHtml = userRecordHtml.concat(`<td data-name='lastName' class='user-record' contenteditable='true'>${user['lastName']}</td>`);
    userRecordHtml = userRecordHtml.concat(`<td data-name='emails' class='user-record' contenteditable='true'>${user['emails'].join(' ')}</td>`);
    var birthday = (new Date(user['birthday'])).toISOString().substring(0, 10);
    userRecordHtml = userRecordHtml.concat(`<td data-name='birthday' class='user-record' contenteditable='true'>${birthday}</td>`);
    userRecordHtml = userRecordHtml.concat(`<td data-name='gender' class='user-record' contenteditable='true'>${user['gender']}</td>`);
    userRecordHtml = userRecordHtml.concat("<td class='user-remove'>X</td>")
    return userRecordHtml;
}

updateUser = function (event) {
    event.preventDefault();
    var id = $(this).closest(".user-head").attr('id');
    var data = {};
    $("#" + id).children('td').each(function (i) {
        if ($(this).attr("data-name") == "emails") {
            data[$(this).attr("data-name")] = $(this).text().split(" ");
        } else if ($(this).attr("data-name") !== undefined) {
            data[$(this).attr("data-name")] = $(this).text();
        }
    });
    var jsonForm = JSON.stringify(data);
    $.ajax({
        url: "/user/" + id,
        type: "PUT",
        encicon: 'multipart/form-data',
        processData: false,
        contentType: "application/json",
        data: jsonForm,
        success: function (result) {
            swal({
                position: 'top-right',
                icon: 'success',
                title: 'User updated',
                button: false,
                timer: 1500
            });
        },
        error: function (xhr, resp, text) {
            swal({
                position: 'top-right',
                icon: 'error',
                title: 'Изменения не сохранены',
                button: false,
                timer: 1500
            })
        }
    })
};

removeUser = function (event) {
    event.preventDefault();
    var id = $(this).closest(".user-head").attr('id');
    $.ajax({
        url: "/user/" + id,
        type: "DELETE",
        success: function (result) {
            swal({
                position: 'top-right',
                icon: 'success',
                title: 'User removed',
                button: false,
                timer: 1500
            });
            $("#" + id).remove();
        },
        error: function (xhr, resp, text) {
            swal({
                position: 'top-right',
                icon: 'error',
                title: 'Error',
                button: false,
                timer: 1500
            })
        }
    })
};


paginateUsers = function (event) {
    event.preventDefault();
    var pageNumber = $(this).attr("data-page");
    $.ajax({
        url: "/index/" + pageNumber,
        type: "GET",
        success: function (result) {
            $('#users-paginate-button-block').remove();
            document.getElementById("users-table-body").innerHTML = document.getElementById("users-table-body").innerHTML.concat(result);
            $('#users-paginate-button-block').detach().insertAfter("#users-table");
            $(".user-record").on("blur", updateUser);
            $(".user-remove").on("click", removeUser);
            $("#users-paginate").on("click", paginateUsers);

        },
        error: function (xhr, resp, text) {
            console.log("err")
        }
    })
};

$(document).ready(function () {
    $("#add-user").on("click", addUser);
    $(".user-record").on("blur", updateUser);
    $(".user-remove").on("click", removeUser);
    $("#users-paginate").on("click", paginateUsers);
    $("#datetimepicker").datetimepicker({format: 'yyyy-mm-dd'});
});
