Simple CRUD Paginate web app
============

    This is small web-app represents basic REST operations.  
    It's allows to CRUD users details in Postgres DB.  
    Owner can create user based on form.  
    Read, Update, Delete operations working based on blur event.  
    So you could simply edit User directly in the table.
    
    Front-end designed to interact both with JSON & HTML server responses.
    

Clone repository. Execute in bash:  

mvn clean & mvn package (or use wrapper)  
cd  target/  
java -jar com-service-1.0-SNAPSHOT.jar  

Open in browser localhost:8080

Pray! :)

Requirements
--------------------
- git
- maven
- java 8
- Postgres 9.6

License
=======

GNU General Public License